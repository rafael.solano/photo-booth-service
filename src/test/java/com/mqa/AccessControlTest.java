package com.mqa;

import java.util.List;
import java.util.Map;
import com.mqa.model.Role;
import com.mqa.model.Status;
import com.mqa.model.User;
import com.mqa.model.UserWrapper;
import com.mqa.service.dto.UserSubmissionDTO;
import com.mqa.util.ErrorEnum;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

public class AccessControlTest extends BaseTest {
    private Logger logger = Logger.getLogger(AccessControlTest.class);    
    public static final String NAME="ILP-ONBASE";
    public static final String LAST_NAME="TEST-USER";


    @Override
    @Before
    public void setUp()  {
        super.setUp();
    }

    @Test
    public void test0004CRUD() throws Exception {        
        List<Role> roleList = ServiceClient.getRoleList(mvc);
        List<Status> statusList = ServiceClient.getStatusList(mvc);
        Map<String,String> headers = Map.ofEntries(rootAuthorization);

        UserSubmissionDTO userDTO = new UserSubmissionDTO(
            "LOREM IPSUM",
            "435A45SAS34#!b",
            "LOREM",
            "IPSUM",
            statusList.get(0).getId(),
            roleList.get(0).getId()
        );

        User createdUser = MockClient.post(
            mvc, 
            "/service/acl/user/",
            userDTO,
            UserWrapper.class,
            MockMvcResultMatchers.status().isCreated(),
            headers
        ).getEntity();

        String name = createdUser.getName();

        Assert.assertNotNull(name);

        User modifiedUser = MockClient.put(
            mvc, "/service/acl/user/"+name,
            new UserSubmissionDTO(
                userDTO.getName(),
                userDTO.getPassword(),
                userDTO.getLastName().toLowerCase(),
                userDTO.getFirstName().toUpperCase(),
                statusList.get(1).getId(),            
                roleList.get(1).getId()
            )   ,
            UserWrapper.class,
            MockMvcResultMatchers.status().isOk(),
            headers
        ).getEntity();

        User fetchedUser = ServiceClient.findUser(mvc, name);
        User fetchedUser2 = ServiceClient.findUser(mvc, modifiedUser.getName());

        Assert.assertNotNull(fetchedUser2);
        logger.debug(modifiedUser);
        logger.debug(fetchedUser);
        
        MockClient.delete(mvc, "/service/acl/user/" +name, MockMvcResultMatchers.status().isOk(), headers);
        MockClient.delete(mvc, "/service/acl/user/" +name, MockMvcResultMatchers.status().isNoContent(), headers);

        Assert.assertTrue(modifiedUser.equals(fetchedUser));
        Assert.assertTrue(!fetchedUser.equals(createdUser));

    }       

    @Test
    public void test0005FailedCreation() throws Exception {    
        Map<String,String> headers = Map.ofEntries(rootAuthorization);
        UserSubmissionDTO userDTO = new UserSubmissionDTO();
        UserWrapper response = MockClient.post(
            mvc,
            "/service/acl/user/",
            userDTO,
            UserWrapper.class,
            MockMvcResultMatchers.status().isBadRequest(),
            headers
        );    
        
        Map<String,ErrorEnum> validationMessages = response.getValidationMessages();
        
        Assert.assertTrue(validationMessages.size() > 0);
        logger.debug(validationMessages);
    }

    @Test
    public void test0006FailedUpdate() throws Exception {    
        Map<String,String> headers = Map.ofEntries(rootAuthorization);
        List<Role> roleList = ServiceClient.getRoleList(mvc);
        List<Status> statusList = ServiceClient.getStatusList(mvc);

        UserSubmissionDTO dto = new UserSubmissionDTO(
            "LOREM IPSUM",
            "435A45SAS34#!b",
            "LOREM",
            "IPSUM",
            statusList.get(0).getId(),
            roleList.get(0).getId()
        );

        User createdUser = MockClient.post(
            mvc,
            "/service/acl/user/",
            dto,
            UserWrapper.class,
            MockMvcResultMatchers.status().isCreated(),
            headers
        ).getEntity();    

        String name = createdUser.getName();

        createdUser.setName(" ");
        UserWrapper response = MockClient.put(
            mvc, "/service/acl/user/"+name,
            createdUser,
            UserWrapper.class,
            MockMvcResultMatchers.status().isBadRequest(),
            headers
        );  
        Map<String,ErrorEnum> validationMessages = response.getValidationMessages();
        
        Assert.assertTrue(validationMessages.size() > 0);
        logger.debug(validationMessages);       
        
        MockClient.delete(mvc, "/service/acl/user/" + name, MockMvcResultMatchers.status().isOk(), headers);        
    } 
    
    @Test
    public void test0007FailedDuplication() throws Exception {  
        Map<String,String> headers = Map.ofEntries(rootAuthorization);
        List<Role> roleList = ServiceClient.getRoleList(mvc);
        List<Status> statusList = ServiceClient.getStatusList(mvc);
        UserSubmissionDTO dto = new UserSubmissionDTO(
            "LOREM IPSUM",
            "435A45SAS34#!b",
            "LOREM",
            "IPSUM",
            statusList.get(0).getId(),
            roleList.get(0).getId()
        );

        User createdUser = MockClient.post(mvc, "/service/acl/user/", dto, UserWrapper.class, MockMvcResultMatchers.status().isCreated(), headers).getEntity();              
        UserWrapper duplicatedUser = MockClient.post(mvc, "/service/acl/user/", dto, UserWrapper.class, MockMvcResultMatchers.status().isConflict(), headers);              
        MockClient.delete(mvc, "/service/acl/user/" + createdUser.getName(), MockMvcResultMatchers.status().isOk(), headers);
        Map<String, ErrorEnum> validationMessages = duplicatedUser.getValidationMessages();
        ErrorEnum nameError = validationMessages.get("name");
        Assert.assertEquals(nameError, ErrorEnum.DUPLICATED_NAME);
    }
      
}
