package com.mqa.model;

import java.util.Map;

import com.mqa.util.ErrorEnum;

public class ResponseWrapper<T> {
    T entity;
    Map<String,ErrorEnum> validationMessages;
    String exception;

    public ResponseWrapper() {
    }

    
    public T getEntity() {
        return entity;
    }

    public Map<String, ErrorEnum> getValidationMessages() {
        return validationMessages;
    }


    public void setEntity(T entity) {
        this.entity = entity;
    }


    public void setValidationMessages(Map<String, ErrorEnum> validationMessages) {
        this.validationMessages = validationMessages;
    }

    public String getException() {
        return exception;
    }
    
}
