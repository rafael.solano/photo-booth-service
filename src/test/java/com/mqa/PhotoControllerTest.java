package com.mqa;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.Map;

import com.mqa.model.Photo;
import com.mqa.model.PhotoList;
import com.mqa.model.PhotoListWrapper;
import com.mqa.model.PhotoWrapper;
import com.mqa.service.dto.PhotoSubmissionDTO;

public class PhotoControllerTest extends  BaseTest  {
    static final Logger logger = Logger.getLogger(PhotoControllerTest.class);    

    @Override
    @Before
    public void setUp()  {
        super.setUp();
    }    

    private String getRawBytes() throws IOException {
        InputStream stream  = PhotoControllerTest.class.getResourceAsStream("/rawbytes.txt");
        int available = stream.available();
        byte[] rawBytesImageArray = new byte[available];

        stream.read(rawBytesImageArray);
        return Base64.getEncoder().encodeToString(rawBytesImageArray);
    }

    @Test
    public void test0001CRUD() throws Exception{
        String rawBytesImage = getRawBytes();
        PhotoSubmissionDTO dto = new PhotoSubmissionDTO("test.root", rawBytesImage, "emojis");
        Map<String,String> headers = Map.ofEntries(rootAuthorization);
        
        Photo photo = MockClient.post(
            mvc, 
            "/service/album/photo/",
            dto,
            PhotoWrapper.class,
            MockMvcResultMatchers.status().isCreated(),
            headers
        ).getEntity();       

        MockClient.get(
            mvc, 
            "/service/album/photo/" + photo.getId(),
            PhotoWrapper.class,
            MockMvcResultMatchers.status().isOk(),
            headers
        );

        PhotoList photos = MockClient.get(
            mvc, 
            "/service/album/photos/",
            PhotoListWrapper.class,
            MockMvcResultMatchers.status().isOk(),
            headers
        ).getEntity();
        Assert.assertTrue(photos.size() > 0);
        
        MockClient.delete(
            mvc, 
            "/service/album/photo/" + photo.getId(),
            MockMvcResultMatchers.status().isOk(),
            headers
        );

        logger.debug(photo.getId());
        MockClient.delete(
            mvc, 
            "/service/album/photo/" + photo.getId(),
            MockMvcResultMatchers.status().isNoContent(),
            headers
        );        
    }
}
