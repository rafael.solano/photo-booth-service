package com.mqa;

import com.mqa.service.ServiceException;

import com.mqa.service.UserManagerService;
import com.mqa.service.dto.UserSubmissionDTO;
import com.mqa.model.User;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;


@RunWith(SpringRunner.class)
@SpringBootTest
public class EnvironmentSetupTest {
    private static Logger logger = Logger.getLogger(EnvironmentSetupTest.class);

    @Autowired
    UserManagerService userManagementService;

    public void createTestUser(String role, String status, String name, String password, String firstName, String lastName) throws ServiceException {
        User user = userManagementService.findById(name);

        if(user == null) {
            userManagementService.createUser(
                (new UserSubmissionDTO(
                    name,
                    password,
                    firstName,
                    lastName,
                    status,
                    role
                )).toUser()
            );
            logger.debug(String.format("%s created", name));
        } else {
            logger.warn(String.format("%s already exists", name));
        }
        
    }

    @Test()
    public void createTestUsers() throws ServiceException {
        createTestUser("Administrador", "Activo", "test.root", "Vasorange#1", "Test", "Root");
        createTestUser("Supervisor", "Activo", "test.supervisor", "Vasorange#1", "Test", "Supervisor");
        createTestUser("Operativo", "Activo", "test.operator", "Vasorange#1", "Test", "Operator");
    }
}
