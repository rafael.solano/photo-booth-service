package com.mqa;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mqa.model.Role;
import com.mqa.model.RoleListWrapper;
import com.mqa.model.Status;
import com.mqa.model.StatusListWrapper;
import com.mqa.model.User;
import com.mqa.model.UserWrapper;
import com.mqa.service.dto.UserSubmissionDTO;

import org.apache.log4j.Logger;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.ResultMatcher;

public class ServiceClient {    
    static Logger logger = Logger.getLogger(ServiceClient.class);

    public static List<Role> getRoleList(MockMvc mvc) throws Exception {
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/service/acl/roles"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn();        
        List<Role> roleList = (List<Role>) MockClient.mapFromJson(result.getResponse().getContentAsString(), RoleListWrapper.class).getEntity();        
        return roleList;
    }

    public static  List<Status> getStatusList(MockMvc mvc) throws Exception {
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/service/acl/status"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn();
        List<Status> statusList = (List<Status>) MockClient.mapFromJson(result.getResponse().getContentAsString(), StatusListWrapper.class).getEntity();       
        return statusList;
    }

    public static User createUser(MockMvc mvc, UserSubmissionDTO user) throws JsonProcessingException, Exception {
       return createUser(mvc, user, MockMvcResultMatchers.status().isCreated());
    }

    public static User createUser(MockMvc mvc, UserSubmissionDTO user, ResultMatcher expected) throws JsonProcessingException, Exception {
        return MockClient.post(
            mvc, "/acl/user/",
            user,
            UserWrapper.class,
            expected
        ).getEntity();
    }

    public static User updateUser(MockMvc mvc, UserSubmissionDTO user, Integer id) throws JsonProcessingException, Exception {
        return MockClient.put(
            mvc, "/acl/user/" + id,
            user,
            UserWrapper.class,
            MockMvcResultMatchers.status().isOk()
        ).getEntity();       
    }
   
    public static User findUser(MockMvc mvc, String name) throws Exception {
        MvcResult result = mvc.perform(MockMvcRequestBuilders.get("/service/acl/user/" + name))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn();
        
        User fetchedUser = (User) MockClient.mapFromJson(result.getResponse().getContentAsString(), UserWrapper.class).getEntity();
        return fetchedUser;
    }  

    public static void deleteUser(MockMvc mvc, Integer id, ResultMatcher expected) throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/service/acl/user/" + id))
            .andExpect(expected)
            .andReturn();
    } 
  
    public static String authenticate(MockMvc mvc, String userName, String password, ResultMatcher match) throws Exception {
        String credentials = userName + ":" + password;
        String authorizationHeader = new String(Base64.getEncoder().encode(credentials.getBytes()));
        Map<String,String> headers = Map.ofEntries(Map.entry("Authorization", "Basic " +authorizationHeader));
        return MockClient.getText(mvc, "/service/auth/login", headers,  match);
    }    

    // MockMvcResultMatchers.status().isOk()
    public static String authenticate(MockMvc mvc, String userName, String password) throws Exception {
        return authenticate(mvc, userName, password, MockMvcResultMatchers.status().isOk());
    }       

    public static String bearkerToken(MockMvc mvc, String userName, String password) throws Exception {
        String response = authenticate(mvc, userName, password);
        return response.substring(response.lastIndexOf('|')+1);
    }
}
