package com.mqa;

import java.util.List;
import java.util.Map;

import com.mqa.model.PasswordSubmissionWrapper;
import com.mqa.model.Role;
import com.mqa.model.Status;
import com.mqa.model.User;
import com.mqa.model.UserWrapper;
import com.mqa.service.UserManagementServiceJPAImpl;
import com.mqa.service.dto.PasswordSubmissionDTO;
import com.mqa.service.dto.UserSubmissionDTO;

import org.apache.commons.lang3.RandomUtils;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

public class AuthenticationTest extends BaseTest{
    static Logger logger = Logger.getLogger(AuthenticationTest.class);

    @Autowired
    UserManagementServiceJPAImpl userManagementService;

    @Override
    @Before
    public void setUp() {
        super.setUp();
    }


    @Test
    public void test0001OperatorLimitedPower() throws Exception {
        // Create test user with operator role
        List<Role> roleList = ServiceClient.getRoleList(mvc);
        List<Status> statusList = ServiceClient.getStatusList(mvc);
        Map<String,String> headers = Map.ofEntries(operatorAuthorization);
        UserSubmissionDTO userDTO = new UserSubmissionDTO(
            "LOREM IPSUM",
            "435A45SAS34#!b",
            "LOREM",
            "IPSUM",
            statusList.get(0).getId(),
            roleList.get(0).getId()            
        );


       MockClient.post(
            mvc, 
            "/service/acl/user/",
            userDTO,
            UserWrapper.class,
            MockMvcResultMatchers.status().isForbidden(),
            headers
        );

    }

    @Test
    public void test0002UsersCanChangePassword() throws Exception {
        Map<String,String> rootHeaders = Map.ofEntries(rootAuthorization);
        String userName = "LOREM_IPSUM " + RandomUtils.nextLong();
        String firstPassword = "435A45SAS34#!b";
        String secondPassword ="3341abyOO12$$";

        UserSubmissionDTO userDTO = new UserSubmissionDTO(
            userName,
            firstPassword,
            "LOREM",
            "IPSUM",
            "Activo",
            "Operativo"            
        );        

        User createdUser = MockClient.post(
            mvc, 
            "/service/acl/user/",
            userDTO,
            UserWrapper.class,
            MockMvcResultMatchers.status().isCreated(),
            rootHeaders
        ).getEntity();

        try {
            String token = ServiceClient.bearkerToken(mvc, userName, firstPassword);
            PasswordSubmissionDTO passwordSubmissionDTO = new PasswordSubmissionDTO(
                secondPassword,
                secondPassword,
                createdUser.getName(),
                token
            );
            
            MockClient.post(
                mvc, 
                "/service/auth/change-password",
                passwordSubmissionDTO,
                PasswordSubmissionWrapper.class,
                MockMvcResultMatchers.status().isOk(),
                Map.ofEntries(Map.entry(
                    AUTHORIZATION_HEADER, 
                    BEARER +' ' + token
                ))
            );          
            
            token = ServiceClient.bearkerToken(mvc, userName, secondPassword);

            passwordSubmissionDTO = new PasswordSubmissionDTO(
                secondPassword+'1',
                secondPassword+'2',
                createdUser.getName(),
                token
            );            

             MockClient.post(
                mvc, 
                "/service/auth/change-password",
                passwordSubmissionDTO,
                Map.class,
                MockMvcResultMatchers.status().isBadRequest(),
                Map.ofEntries(Map.entry(
                    AUTHORIZATION_HEADER, 
                    BEARER +' ' + token
                ))
            );             

            
        }finally {
            userManagementService.deleteUser(createdUser.getName());
        }
    }    
}
