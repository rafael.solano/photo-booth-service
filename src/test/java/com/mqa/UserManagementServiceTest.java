package com.mqa;

import java.util.Map;
import com.mqa.service.SecurityAttributeService;
import com.mqa.service.UserManagerService;
import com.mqa.service.dto.UserSubmissionDTO;
import com.mqa.util.ErrorEnum;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.jupiter.api.Assertions.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest
public class UserManagementServiceTest {
    private static Logger logger =Logger.getLogger(UserManagementServiceTest.class);

    @Autowired
    SecurityAttributeService securityAttributeService;
    
    @Autowired
    UserManagerService userManagementService;

    @Test
    public void testValidateAllFields()  {
        final int CONSTRAINED_FIELDS_COUNT = 6;
        
        UserSubmissionDTO dto = new UserSubmissionDTO();
        Map<String,ErrorEnum> validationMessages = userManagementService.validate(dto.toUser(), securityAttributeService);
        logger.debug(validationMessages);
        Assert.assertEquals(validationMessages.size(), CONSTRAINED_FIELDS_COUNT);        
        
        dto.setFirstName("LOREM");
        validationMessages = userManagementService.validate(dto.toUser(), securityAttributeService);
        logger.debug(validationMessages);
        Assert.assertEquals(validationMessages.size(), CONSTRAINED_FIELDS_COUNT-1);        

        dto.setLastName("IPSUM");
        validationMessages = userManagementService.validate(dto.toUser(), securityAttributeService);
        logger.debug(validationMessages);        
        Assert.assertEquals(validationMessages.size(), CONSTRAINED_FIELDS_COUNT-2);                

        dto.setStatus("Deleted");
        validationMessages = userManagementService.validate(dto.toUser(), securityAttributeService);
        logger.debug(validationMessages);        
        Assert.assertEquals(validationMessages.size(), CONSTRAINED_FIELDS_COUNT-3);

        dto.setRole("Administrador");
        validationMessages = userManagementService.validate(dto.toUser(), securityAttributeService);
        logger.debug(validationMessages);        
        Assert.assertEquals(validationMessages.size(), CONSTRAINED_FIELDS_COUNT-4);     
        
        dto.setName("USERNAME");
        validationMessages = userManagementService.validate(dto.toUser(), securityAttributeService);
        logger.debug(validationMessages);        
        Assert.assertEquals(validationMessages.size(), CONSTRAINED_FIELDS_COUNT-5);           

        dto.setPassword("435A45SAS34#!b");
        validationMessages = userManagementService.validate(dto.toUser(), securityAttributeService);
        logger.debug(validationMessages);        
        Assert.assertEquals(validationMessages.size(), CONSTRAINED_FIELDS_COUNT-6);      

    }

    @Test
    public void testValidateChoicesFields()  {
        UserSubmissionDTO dto = new UserSubmissionDTO(
            "LOREM IPSUM",
            "aabbc",
            "LOREM",
            "IPSUM",
            "no status",
            "no role"
        );

        Map<String, ErrorEnum> validationMessages = userManagementService.validate(dto.toUser(), securityAttributeService);
        ErrorEnum statusError = validationMessages.get("status");
        ErrorEnum roleError = validationMessages.get("role");
        ErrorEnum passwordError = validationMessages.get("password");
        assertEquals(statusError, ErrorEnum.MISSING_CHOICE);
        assertEquals(roleError, ErrorEnum.MISSING_CHOICE);
        assertEquals(passwordError, ErrorEnum.UNSAFE_PASSWORD);
    }


}
