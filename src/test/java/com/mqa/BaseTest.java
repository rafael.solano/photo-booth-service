package com.mqa;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
import java.util.Map.Entry;
import com.mqa.environment.SystemProperty;
import com.mqa.util.Properties;

import org.apache.log4j.Logger;
import org.junit.FixMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.junit.runners.MethodSorters;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
@TestPropertySource(locations = "classpath:application.properties")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public abstract class BaseTest {
    private static Logger logger = Logger.getLogger(BaseTest.class);
    protected MockMvc mvc;
    @Autowired
    WebApplicationContext webApplicationContext;
        
    Entry<String, String> rootAuthorization;
    Entry<String, String> operatorAuthorization;
    Entry<String, String> supervisorAuthorization;

    public static final String AUTHORIZATION_HEADER="Authorization";
    public static final String BEARER="Bearer";


    /* These three users must manually created before running these tests */
    public String ROOT_USER="ROOT_USER";
    public String ROOT_PASSWORD="ROOT_PASSWORD";
    public String OPERATOR_USER="OPERATOR_USER";
    public String OPERATOR_PASSWORD="OPERATOR_PASSWORD";
    public String SUPERVISOR_USER="SUPERVISOR_USER";
    public String SUPERVISOR_PASSWORD="SUPERVISOR_PASSWORD";

    protected void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).apply(springSecurity()).build();

        try {
            Properties properties = Properties.readResource(BaseTest.class);
            rootAuthorization = Map.entry(
                AUTHORIZATION_HEADER, 
                BEARER +' ' + ServiceClient.bearkerToken(mvc, properties.getProperty(ROOT_USER), properties.get(ROOT_PASSWORD).toString())
            );
            operatorAuthorization =  Map.entry(
                AUTHORIZATION_HEADER,
                BEARER +' ' + ServiceClient.bearkerToken(mvc, properties.getProperty(OPERATOR_USER), properties.get(OPERATOR_PASSWORD).toString())
            );
            supervisorAuthorization = Map.entry(
                AUTHORIZATION_HEADER,
                BEARER +' ' + ServiceClient.bearkerToken(mvc, properties.getProperty(SUPERVISOR_USER), properties.get(SUPERVISOR_PASSWORD).toString())
            );
        }catch(Exception e) {
            logger.error("Can't load properties", e);
        }        
    }
  
    public static Connection getConnection() throws SQLException {
        String url = SystemProperty.getEnv("ONBASE_MSSQL_DB_URL", null);
        String user = SystemProperty.getEnv("ONBASE_MSSQL_USER", null);
        String password = SystemProperty.getEnv("ONBASE_MSSQL_PASSWORD", null);

        return DriverManager.getConnection(url, user, password);
    }
}
