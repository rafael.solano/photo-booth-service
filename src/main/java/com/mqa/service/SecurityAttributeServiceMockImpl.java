package com.mqa.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.mqa.model.Role;
import com.mqa.model.Status;

import org.springframework.stereotype.Service;

@Service
public class SecurityAttributeServiceMockImpl implements SecurityAttributeService{
    public List<Role> findAllRoles() {
        return roleList.stream().map(role -> new Role(role.getId(), role.getName())).collect(Collectors.toList());
    }

    public List<Status> findAllStatus() {
        return statusList.stream().map(status -> new Status(status.getId(), status.getName())).collect(Collectors.toList());
    }    

    public Role findRole(String id) {
        Optional<Role> role = roleList.stream().filter(item -> {return item.getId().equals(id); }).findFirst();
        return role.isPresent() ? null : role.get();
    }
    public Status findStatus(String id) {
        Optional<Status> status = statusList.stream().filter(item -> {return item.getId().equals(id); }).findFirst();
        return status.isPresent() ? null : status.get();
    }      
}
