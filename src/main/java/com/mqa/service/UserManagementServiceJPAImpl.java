package com.mqa.service;

import java.lang.reflect.InvocationTargetException;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.transaction.Transactional;

import com.mqa.model.Role;
import com.mqa.model.Status;
import com.mqa.model.User;
import com.mqa.security.TokenFactory;
import com.mqa.service.dto.UserQueryDTO;
import com.mqa.util.Aggregator;
import com.mqa.util.ErrorEnum;
import com.mqa.util.Formatters;
import com.mqa.util.Pair;
import com.mqa.util.Triplet;
import com.mqa.util.Validators;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.stereotype.Service;

@Service
public class UserManagementServiceJPAImpl implements UserManagerService {
    
    public Charset passwordCharset;

    @PersistenceContext
    private EntityManager entityManager;
    
    public static Map<String, ErrorEnum> validate(User user, List<Role> roles, List<Status> status) {  
        List<String> roleIds = roles.stream().map(item -> item.getId()).collect(Collectors.toList());
        List<String> statusIds = status.stream().map(item -> item.getId()).collect(Collectors.toList());

        Map<String,ErrorEnum> textValidationMap = Aggregator.sequential(
            Arrays.asList(
                Pair.of("role",  user.getRole()),
                Pair.of("status", user.getStatus()),
                Pair.of("name", user.getName()),
                Pair.of("lastName", user.getLastName()),
                Pair.of("firstName", user.getFirstName())
            ),
            (Map<String,ErrorEnum> map, Pair<String,String> pair) -> {
                return Validators.validateString(pair.a, pair.b, map);
            },
            new HashMap<String, ErrorEnum>()
        );

        Map<String,ErrorEnum> choiceValidationMap = Aggregator.sequential(
            Arrays.asList(
                Triplet.of("role",  user.getRole(), roleIds),
                Triplet.of("status", user.getStatus(), statusIds)
            ),
            (Map<String,ErrorEnum> map, Triplet<String, String, List<String>> triplet) -> {
                return Validators.ChoiceValidator.validateChoice(triplet.a, triplet.b, triplet.c, map);
            },
            new HashMap<String, ErrorEnum>()
        );        

        Map<String,ErrorEnum> byteArrayValidationMap = Aggregator.sequential(
            Arrays.asList(
                Pair.of("password", user.getPassword())
            ),
            (Map<String,ErrorEnum> map, Pair<String,byte[]> pair) -> {
                return Validators.validatePassword(pair.a, pair.b, map);
            },            
            new HashMap<String,ErrorEnum>()
        );

        return  Arrays.asList(
            textValidationMap,
            choiceValidationMap,
            byteArrayValidationMap
        ).stream().reduce(
            new HashMap<String,ErrorEnum>(),
            (Map<String,ErrorEnum> acumulator, Map<String,ErrorEnum> element) -> {
                acumulator.putAll(element);
                return acumulator;
            }
        );
        
    }   

    public Map<String, ErrorEnum> validate(User user, SecurityAttributeService securityAttributeService) {  
        return validate(user, securityAttributeService.findAllRoles(), securityAttributeService.findAllStatus());
    }

    public UserManagementServiceJPAImpl() {
        passwordCharset = Charset.forName("UTF-8");
    }

    @Transactional
    public String createUser(User user) throws ServiceException {
        byte[] salt = TokenFactory.randomBytes(16);
        try {
            byte[] password = Formatters.encodePassword(user.getPassword(), salt);
            user.setSalt(salt);
            user.setPassword(password);
            entityManager.persist(user);
            entityManager.flush();
            return user.getName();
    
        } catch (InvalidKeyException | SignatureException | NoSuchAlgorithmException | PersistenceException e) {
            throw new ServiceException("Can't create user", e);
        }
        
    }

    @Transactional
    public String updateUser(User user) throws ServiceException {
        User transaction = entityManager.find(User.class, user.getName());
        String name = transaction.getName();
        byte[] salt = transaction.getSalt();
        byte[] password = transaction.getPassword();
        
        try {

            PropertyUtils.copyProperties(transaction, user);
            transaction.setName(name);
            transaction.setSalt(salt);
            transaction.setPassword(password);
            entityManager.flush();
            return user.getName();
    
        } catch (PersistenceException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new ServiceException("Can't update user", e);
        }
    }

    @Transactional
    public User deleteUser(String name) throws ServiceException {
        try {
            User transaction = entityManager.find(User.class, name);

            if(transaction != null) {
                entityManager.remove(transaction);
                entityManager.flush();
            }

            return transaction;
        }catch(PersistenceException | IllegalArgumentException e) {
            throw new ServiceException("Can't delete user", e);
        }
    }

    public User findById(String id) throws ServiceException {
        try {
            return entityManager.find(User.class, id);        
        }catch(PersistenceException | IllegalArgumentException  e) {
            throw new ServiceException("Can't find user by id", e);
        }
    }    

    @SuppressWarnings("unchecked")
    public List<User> findAll(int pageNumber, int pageSize)  throws ServiceException {
        try {
            List<User> users = entityManager
            .createQuery("SELECT u FROM User u ORDER BY u.name")
            .setFirstResult((pageNumber - 1) * pageSize)
            .setMaxResults(pageSize)
            .getResultList();


            return users;
        }catch(PersistenceException | IllegalArgumentException e) {
            throw new ServiceException("Can't fetch all users", e);
        }
    }
    
    public int countAllPages(int pageSize) {
        Number count = (Number) entityManager
            .createNamedQuery("SELECT COUNT(*) CNT FROM User")
            .getSingleResult();    

        int records = count.intValue();
        
        if(records == 0)
            return 0;
        else if (records > 0 && records < pageSize)
            return 1;
        
        return records / pageSize + ( (records % pageSize) > 0 ? 1 : 0);
    }

    @SuppressWarnings("unchecked")
    public List<User> findByName(String firstName, String lastName) throws ServiceException {
        try {
            List<User> users = entityManager
                .createQuery("SELECT u FROM User u WHERE name=? AND lastName=?")
                .getResultList();

            return users;
        }catch(PersistenceException | IllegalArgumentException e) {
            throw new ServiceException("Can't fetch user by name", e);
        }
    }    

    private Query widthUserQueryDTO(UserQueryDTO userQueryDTO, int pageSize, EntityManager entityManager) {
        Query query = (new QueryBuilder("User"))
            .addFilter("name", userQueryDTO.getName())
            .addFilter("lastName", userQueryDTO.getLastName())
            .addFilter("status", userQueryDTO.getStatus())
            .addFilter("firstName", userQueryDTO.getFirstName())
            .addFilter("role", userQueryDTO.getRole())
            .addFilter("creationDate", userQueryDTO.getCreationDate())
            .build(entityManager);

        
        if(userQueryDTO.getPageNumber() != null) {
            query
                .setFirstResult((userQueryDTO.getPageNumber()-1) * pageSize)
                .setMaxResults(pageSize);            
        }

        return query;
    }

    @SuppressWarnings("unchecked")
    public List<User> findAll(UserQueryDTO queryDTO, int pageSize) throws ServiceException {
        try {
            Query query = widthUserQueryDTO(queryDTO, pageSize, entityManager);        
            return query.getResultList();
        }catch(IllegalStateException | PersistenceException e)  {
            throw new ServiceException("Can't query user repository", e);
        }
    }

    public Charset getPasswordCharset() {
        return this.passwordCharset;
    }

    @Transactional
    public void changePassword(String name, byte[] password) throws ServiceException {
        User transaction = entityManager.find(User.class, name);
        try {
            transaction.setPassword(Formatters.encodePassword(password, transaction.getSalt()));
            entityManager.flush();
        } catch (InvalidKeyException | SignatureException | NoSuchAlgorithmException | PersistenceException e) {
            throw new ServiceException("Can't change password", e);
        }
        
    }

// HDLADMIN, Vasorange#10205
}