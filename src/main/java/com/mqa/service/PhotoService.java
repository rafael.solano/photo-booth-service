package com.mqa.service;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import com.mqa.model.Photo;
import com.mqa.util.ErrorEnum;

public interface PhotoService {
    @Transactional
    long insertPhoto(Photo photo) throws ServiceException;

    @Transactional
    long updatePhoto(Photo photo) throws ServiceException;    

    Photo find(long photoId) throws ServiceException;
    
    @Transactional
    Photo deletePhoto(long photoId) throws ServiceException;

    List<Photo> findPhotosByUserName(String userName) throws ServiceException;
    
    Map<String, ErrorEnum> validate(Photo photo);
}
