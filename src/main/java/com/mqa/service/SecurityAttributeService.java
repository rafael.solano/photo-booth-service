package com.mqa.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import com.mqa.model.Role;
import com.mqa.model.Status;


public interface SecurityAttributeService {
    public static List<Role> roleList = Collections.unmodifiableList(Arrays.asList(
        new Role("Administrador", "Administrador"),
        new Role("Supervisor", "Supervisor"),
        new Role("Operativo", "Operativo")        
    ));

    public static List<Status> statusList = Collections.unmodifiableList(Arrays.asList(
        new Status("Activo", "Activo"),
        new Status("Deleted", "Deleted"),
        new Status("Inactivo", "Inactivo")
    ));     

    List<Role> findAllRoles();
    List<Status> findAllStatus();
    Role findRole(String id);
    Status findStatus(String id);
}
