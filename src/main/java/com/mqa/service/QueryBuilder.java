package com.mqa.service;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public class QueryBuilder {
    String initialText;
    StringBuilder stringBuilder;
    int conditionCount = 0;
    Map<String,Object> parameters = new HashMap<String, Object>();
    boolean invalidated;

    public QueryBuilder(String entityName) {
        this.stringBuilder = new StringBuilder(String.format("SELECT E FROM %s E", entityName));
    }

    public static boolean provided(String value) {
        return !(value == null || value.isBlank() || value.isEmpty());
    }

    private void startFilter(String name) {
        if(conditionCount == 0) {
            stringBuilder.append(" WHERE ");
        }

        stringBuilder.append(String.format("%s=:%s AND ", name, name));
        conditionCount++;
    }
    public QueryBuilder addFilter(String name, String value) {

        if(provided(value)) {
            startFilter(name);            
            parameters.put(name, value);
        }
        
        return this;
    }

    public QueryBuilder addFilter(String name, Timestamp value) {
        if(value != null) {
            startFilter(name);            
            parameters.put(name, value);
        }

        return this;
    }    

    public QueryBuilder addFilter(String name, Number value) {
        if(value != null) {
            startFilter(name);            
            parameters.put(name, value);
        }

        return this;
    }    
    
    public Query build(EntityManager entityManager) {
        if(conditionCount > 0) {
            stringBuilder.delete(stringBuilder.length()-4, stringBuilder.length());
        }
        String text = stringBuilder.toString();
        Query query = entityManager.createQuery(text);
        Set<String> keySet = parameters.keySet();

        keySet.forEach(parameterName -> {
            query.setParameter(parameterName, parameters.get(parameterName));
        });

        return query;
    }
}
