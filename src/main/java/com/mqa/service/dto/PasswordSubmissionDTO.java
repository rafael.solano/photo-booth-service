package com.mqa.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PasswordSubmissionDTO {
    private String password1;
    private String password2;
    private String name;
    private String token;

    
    public PasswordSubmissionDTO(
        @JsonProperty("password1") String password1,
        @JsonProperty("password2") String password2,
        @JsonProperty("name") String name,
        @JsonProperty("token") String token
    ) {
        this.password1 = password1;
        this.password2 = password2;
        this.name = name;
        this.token = token;
    }

    public String getPassword1() {
        return password1;
    }

    public String getName() {
        return name;
    }

    public String getToken() {
        return token;
    }

    public String getPassword2() {
        return password2;
    }

    
}
