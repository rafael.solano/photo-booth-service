package com.mqa.service.dto;

import java.sql.Timestamp;
import java.util.Arrays;

import com.mqa.util.Constants;
import com.mqa.util.Formatters;
import com.mqa.util.Pair;

public class UserQueryDTO {    
    String name; 
    String lastName; 
    String status; 
    String firstName;
    String role;
    Timestamp creationDate;      
    Integer pageNumber;
    
    private String queryString;

    private void createQueryString() {
        StringBuilder builder = new StringBuilder('?');

        Arrays.asList(
            Pair.of("name", name),
            Pair.of("lastName", lastName),
            Pair.of("status", status),
            Pair.of("firstName", firstName),
            Pair.of("role", role),
            Pair.of("creationDate", creationDate == null ? null : Formatters.timestampFormatter.format(creationDate)),            
            Pair.of("pageNumber", pageNumber == null ? null : pageNumber.toString())
        ).stream()        
        .filter(pair -> {return pair.b != null; })
        .forEach(pair -> {
            builder.append(String.format("%s=%s&", pair.a, Formatters.urlEncode(pair.b)));
        });
    
        if(builder.length() > 1)
            queryString = builder.deleteCharAt(builder.length()-1) .toString();
        else
            queryString = Constants.EMPTY_STRING;
    }
    
    public UserQueryDTO(String name, String lastName, String status, String firstName, String role, Timestamp creationDate, Integer pageNumber) {
        this.name = name;
        this.lastName = lastName;
        this.status = status;
        this.firstName = firstName;
        this.role = role;
        this.creationDate = creationDate;
        this.pageNumber = pageNumber;
        createQueryString();
    }

    public UserQueryDTO(String name, String lastName, String status, String firstName, String role, String creationDate, Integer pageNumber) {
        this.name = name;
        this.lastName = lastName;
        this.status = status;
        this.firstName = firstName;
        this.role = role;
        this.creationDate = Formatters.parseTimestamp(creationDate);
        this.pageNumber = pageNumber;  
        createQueryString();  
    }

    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getStatus() {
        return status;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getRole() {
        return role;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public String getQueryString() {
        return queryString;
    }

}
