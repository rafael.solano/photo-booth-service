package com.mqa.service.dto;

import java.sql.Timestamp;
import java.util.Calendar;

import com.mqa.model.Photo;
import com.mqa.model.User;
import com.mqa.service.ServiceException;
import com.mqa.service.UserManagerService;

import org.apache.tomcat.util.codec.binary.Base64;

public class PhotoSubmissionDTO {
    String userName;
    String rawBytes; //base 64
    String name;

    public PhotoSubmissionDTO() {        
    }

    
    public PhotoSubmissionDTO(String userName, String rawBytes, String name) {
        this.userName = userName;
        this.rawBytes = rawBytes;
        this.name = name;
    }


    public String getRawBytes() {
        return rawBytes;
    }

    public void setRawBytes(String rawBytes) {
        this.rawBytes = rawBytes;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Photo toPhoto(UserManagerService userManagementService, PhotoSubmissionDTO photoSubmission) throws ServiceException {
        User user = userManagementService.findById(photoSubmission.getUserName());
        byte[] rawBytes = photoSubmission.getRawBytes() == null ? null : Base64.decodeBase64(photoSubmission.getRawBytes()) ;
        Photo photo = new Photo(
            new Timestamp(Calendar.getInstance().getTimeInMillis()),
            rawBytes,
            photoSubmission.getName(),
            user
        );

        return photo;

    }
}
