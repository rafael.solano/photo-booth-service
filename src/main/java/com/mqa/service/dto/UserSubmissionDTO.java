package com.mqa.service.dto;

import java.sql.Timestamp;
import java.util.Calendar;

import com.mqa.model.User;
import com.mqa.util.Formatters;
import com.mqa.util.Validators;

public class UserSubmissionDTO {
    String name;
    String password;
    String lastName;
    String status;
    String firstName;
    String role;

    public UserSubmissionDTO() {

    }
    
    public UserSubmissionDTO(String name, String password, String firstName, String lastName, String status, String role) {
        this.name = name;
        this.password = password;
        this.lastName = lastName;
        this.status = status;
        this.firstName = firstName;
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;        
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFirstName() {
        return firstName;        
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
        
    public User toUser() {
        return new User(
            Validators.trim(name), 
            Formatters.passwordBytes(this.getPassword()), 
            null, 
            lastName, 
            firstName, 
            status, 
            role, 
            new Timestamp(Calendar.getInstance().getTimeInMillis())
        );
    }

}
