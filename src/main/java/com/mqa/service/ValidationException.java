package com.mqa.service;

import java.util.Collections;
import java.util.Map;

public class ValidationException extends Exception{
    Map<String,String> validationMessages;

    public ValidationException(String message, Map<String,String> validationMessages) {
        super(message);
        this.validationMessages = Collections.unmodifiableMap(validationMessages);
    }

    public Map<String, String> getValidationMessages() {
        return validationMessages;
    }

    
    
}
