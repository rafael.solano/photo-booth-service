package com.mqa.service;

public class ServiceException extends Exception {
    public ServiceException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public ServiceException(String message) {
        super(message);
    }    
}
