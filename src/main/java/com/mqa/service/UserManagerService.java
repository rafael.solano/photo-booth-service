package com.mqa.service;

import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;
import com.mqa.model.User;
import com.mqa.service.dto.UserQueryDTO;
import com.mqa.util.ErrorEnum;

public interface UserManagerService {
    @Transactional
    String createUser(User user) throws ServiceException; // OK

    @Transactional
    String updateUser(User user) throws ServiceException;  // OK 

    @Transactional
    User deleteUser(String name) throws ServiceException; // OK

    List<User> findAll(int pageNumber, int pageSize) throws ServiceException; // OK

    List<User> findByName(String firstName, String lastName) throws ServiceException; // OK

    public List<User> findAll(UserQueryDTO queryDTO, int pageSize) throws ServiceException; // OK
    
    void changePassword(String name, byte[] password)  throws ServiceException; // OK

    Map<String, ErrorEnum> validate(User user, SecurityAttributeService securityAttributeService);

    User findById(String id) throws ServiceException; // OK

    Charset getPasswordCharset();
}
