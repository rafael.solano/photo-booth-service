package com.mqa.service;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;

import com.mqa.model.Photo;
import com.mqa.model.User;
import com.mqa.util.ErrorEnum;

import org.springframework.stereotype.Service;

@Service
public class PhotoServiceJPAImpl implements PhotoService {
    @PersistenceContext
    private EntityManager entityManager;
    
    @Transactional
    public long insertPhoto(Photo photo) throws ServiceException {
        try {
            entityManager.persist(photo);
            entityManager.flush();
        }catch(PersistenceException  e) {
            throw new ServiceException("Can't insert new photo into repository.", e);
        }
        return photo.getId();
    }    

    @Transactional
    public long updatePhoto(Photo photo) throws ServiceException {
        try {
            Photo transaction = entityManager.find(Photo.class, photo.getId());
            transaction.setRawBytes(photo.getRawBytes());
            entityManager.flush();
            return photo.getId();
    
        }catch(PersistenceException  e) {
            throw new ServiceException("Can't update new photo into repository.", e);
        }
    } 
    
    public Photo find(long photoId) throws ServiceException {
        try {
            return entityManager.find(Photo.class, photoId);    
        }catch(PersistenceException  e) {
            throw new ServiceException("Can't seek photo in the repository.", e);
        }
    }    

    @Transactional
    public Photo deletePhoto(long photoId) throws ServiceException {
        try {
            Photo transaction = entityManager.find(Photo.class, photoId);

            if(transaction != null) {
                entityManager.remove(transaction);
                entityManager.flush();
            }

            return transaction;
        }catch(PersistenceException | IllegalArgumentException e) {
            throw new ServiceException("Can't delete photo", e);
        }
    }

    @Override
    public Map<String, ErrorEnum> validate(Photo photo) {
        Map<String, ErrorEnum> validationMessages = Map.of();
        User user = photo.getUser();
        byte[] rawBytes = photo.getRawBytes();

        if(user == null || user.getName() == null) {
            validationMessages.put("userName", ErrorEnum.USER_NAME_REQUIRED);
        }

        if(photo.getRawBytes() == null || rawBytes.length == 0) {
            validationMessages.put("rawBytes", ErrorEnum.EMPTY_ARRAY);
        }

        return validationMessages;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Photo> findPhotosByUserName(String userName) throws ServiceException {
        try {
            List<Photo> photos = entityManager
                .createNamedQuery("findPhotosByUserName")
                .setParameter(1, userName)
                .getResultList();
            return photos;
        }catch(PersistenceException e) {
            throw new ServiceException("Can't retrieve photos belonging to " + userName, e);
        }
    }  
    
}
