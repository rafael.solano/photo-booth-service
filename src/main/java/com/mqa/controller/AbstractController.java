package com.mqa.controller;

import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.logging.log4j.spi.AbstractLogger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class AbstractController {
    private static final Logger logger = Logger.getLogger(AbstractLogger.class);
    static final String PERSISTENCE_EXCPT_MESG = "Can't perform CRUD operation due to a problem in persistence manager.";
    static final String SUCCESSFUL_UPDATE_MSG = "Entity updated.";
    static final String PASSWORD_KEYWORD = "password";
    static final String PASSWORD1_KEYWORD = "password2";
    static final String PASSWORD2_KEYWORD = "password1";
    static final String NAME_KEYWORD = "name";
    static final String ID_KEYWORD = "id";
    static final String EXCEPTION_MESG_KEYWORD = "exception-message";
    static final String EXCEPTION_KEYWORD="exception";    
    static final String NOTEXISTING_ENTITY_MSG = "Entity not found";
    static final String TOKEN_MISMATCH = "Token mismatch";
    static final String OPERATION_KEYWORD="operation";    
    static final String SERVICE_EXCPT_MESG = "Can't perform CRUD operation because a third party utility threw an exception.";
    static final String POST_KEYWORD ="POST";
    
    public ResponseEntity<Response> reportException(Exception e, Object body, String logMessage) {
        logger.error(logMessage, e);

        return ResponseEntity
            .status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new Response(
                Map.of(),
                String.format(
                    "exception:%s, message: %s",
                    e.getClass().getName(),
                    e.getMessage()
                )
            )); 
    }
}
