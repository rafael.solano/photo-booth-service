package com.mqa.controller;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import com.mqa.model.User;
import com.mqa.service.SecurityAttributeService;
import com.mqa.service.ServiceException;
import com.mqa.service.UserManagerService;
import com.mqa.service.dto.UserQueryDTO;
import com.mqa.service.dto.UserSubmissionDTO;
import com.mqa.util.ErrorEnum;
import com.mqa.util.Formatters;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;

@CrossOrigin(origins = "*", maxAge = 3600,  methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT, RequestMethod.DELETE, RequestMethod.OPTIONS, RequestMethod.HEAD})
@Controller
@RequestMapping("service/acl")
public class AccessControlController extends AbstractController {
    private static Logger logger = Logger.getLogger(AccessControlController.class);
    public static final int DEFAULT_PAGE_SIZE  = 10;

    public static final String USER_URI="/user";
    public static final String USERS_URI="/users";
    public static final String ROLES_URI="/roles";
    public static final String STATUS_URI="/status";
    public static final String USER_BY_ID_URI= USER_URI + "/{id}";

    @Autowired
    UserManagerService userManagementService;

    @Autowired
    SecurityAttributeService securityAttributeService;

    public AccessControlController() {
        logger.debug("Creating " + getClass().getCanonicalName());
    }

    public static Timestamp parseTimestamp(String timestamp) {
        if(timestamp == null || timestamp.isEmpty() || timestamp.isBlank()) {
            return null;
        }

        try {
            return new Timestamp(Formatters.timestampFormatter.parse(timestamp.trim()).getTime());
        }catch(ParseException e) {
            return null;
        }
    }

    @GetMapping(USER_BY_ID_URI)
    public ResponseEntity<Response> get(@PathVariable("id") String id) {

        try {
            
            User user = userManagementService.findById(id);
            if (user == null) {
                return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new Response(Map.of(ID_KEYWORD, NOTEXISTING_ENTITY_MSG)));    
            }


            return ResponseEntity.ok().body(new Response(user));
        }catch(Exception e) {
            return reportException(e, null, PERSISTENCE_EXCPT_MESG);       
        }
    }      

    @DeleteMapping(USER_BY_ID_URI)
    public ResponseEntity<Response> delete(@PathVariable("id") String id) {

        try {
            User user = userManagementService.deleteUser(id);
            if(user != null) {
                return ResponseEntity.status(HttpStatus.OK).body(new Response(id));
            }

            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new Response(id));
        }catch(Exception e) {
            return reportException(e, null, PERSISTENCE_EXCPT_MESG);       
        }
    } 

    @PostMapping(USER_URI)
    public ResponseEntity<Response> post(@RequestBody UserSubmissionDTO body) {
        String id;
        User user = body.toUser();

        try {            
            Map<String, ErrorEnum> validationMessages = userManagementService.validate(user, securityAttributeService);

            if(validationMessages.size() == 0) {
                if(userManagementService.findById(body.getName()) == null) {
                    id = userManagementService.createUser(user);
                    body.setName(id);
                    return ResponseEntity.status(HttpStatus.CREATED).body(new Response(user));    
                }

                validationMessages.put("name", ErrorEnum.DUPLICATED_NAME);
                return ResponseEntity.status(HttpStatus.CONFLICT).body(new Response(validationMessages, user));
            }

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(validationMessages, user));

        }catch(Exception e) {
            return reportException(e, user, PERSISTENCE_EXCPT_MESG);
        }
    }
        
    @PutMapping(USER_BY_ID_URI)
    public ResponseEntity<Response> put(@PathVariable("id") String id, @RequestBody UserSubmissionDTO body) {

        try {
            User user = body.toUser();
            Map<String, ErrorEnum> validationMessages = userManagementService.validate(user, securityAttributeService);

            if(validationMessages.size() == 0) {
                User current = userManagementService.findById(id);

                if(current == null) {
                    return ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .body(new Response(Map.of(ID_KEYWORD, NOTEXISTING_ENTITY_MSG)));    
    
                }

                user.setName(id);
                user.setSalt(current.getSalt());
                id = userManagementService.updateUser(user);
                
                return ResponseEntity.status(HttpStatus.OK).body(new Response(user));
    
            }

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(validationMessages, user));            
        } catch(ServiceException e) {
            return reportException(e, body, SERVICE_EXCPT_MESG);            
        }
        catch(Exception e) {
            return reportException(e, body, PERSISTENCE_EXCPT_MESG);
        }
    } 

    @GetMapping(USER_URI+"/all")
    public ResponseEntity<Response> all(@RequestParam(name="page", required=true) int pageNumber) {

        try {
            List<User> users = userManagementService.findAll(pageNumber, DEFAULT_PAGE_SIZE);

            return ResponseEntity.ok().body(new Response(users));
        }catch(Exception e) {
            return reportException(e, null, PERSISTENCE_EXCPT_MESG);       
        }
    } 

    @GetMapping(USERS_URI+"/filtered")
    public ResponseEntity<Response> filtered(
        @RequestParam(name="pageNumber", required = false) Integer pageNumber,
        @RequestParam(name="status", required=false) String status,
        @RequestParam(name="role", required=false) String role,        
        @RequestParam(name="name", required=false) String name,
        @RequestParam(name="lastName", required=false) String lastName,
        @RequestParam(name="firstName", required=false) String firstName,        
        @RequestParam(name="creationDate", required=false) String creationDate      
    ) {
        
        try {
            UserQueryDTO userQuery = new UserQueryDTO(
                name,
                lastName,
                status,
                firstName,
                role,
                parseTimestamp(creationDate),
                pageNumber
            );
            logger.debug(userQuery.getQueryString());
            List<User> users = userManagementService.findAll(userQuery, DEFAULT_PAGE_SIZE);

            return ResponseEntity.ok().body(new Response(users));
        }catch(Exception e) {
            return reportException(e, null, PERSISTENCE_EXCPT_MESG);       
        }
    } 

    @GetMapping(ROLES_URI)
    public ResponseEntity<Response> roles() {

        return ResponseEntity.ok().body(
            new Response(securityAttributeService.findAllRoles())
        );
    }    
    
    @GetMapping(STATUS_URI)
    public ResponseEntity<Response> status() {

        return ResponseEntity.ok().body(
            new Response(securityAttributeService.findAllStatus())
        );
    }     
}
