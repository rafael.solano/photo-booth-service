package com.mqa.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mqa.util.ErrorEnum;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class AuthenticationRequestFilter extends OncePerRequestFilter{
	private static Logger logger = Logger.getLogger(AuthenticationRequestFilter.class);
	
	public static class Authority implements GrantedAuthority{
		/**
		 * 
		 */
		private static final long serialVersionUID = 3314043576575820863L;
		String authority;
		public Authority(String authority) { this.authority = authority; }
		public String getAuthority() {
			return authority;
		}		
	}
	
	public static final Pattern BEARER_AUTHORIZATION_PATTERN = Pattern.compile("Bearer\\s+\\S+");
	@Autowired
	AuthControllerConfig authConfig;
	
	private  String readToken(String token)  {
		String errorMessage = "Unhandled exception caugth while reading authentication token.";

		try {
			return authConfig.tokenFactory().decode(token);
		} catch (Exception e) {		
			logger.warn(errorMessage, e);
			return null;
		}
		
	}
	
	private void setAuthentication(String token) {
		StringTokenizer parts = new StringTokenizer(token,"|");
		String userId = parts.nextToken();
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		
		parts.nextToken(); //Skips timestamp
		
		while(parts.hasMoreTokens()) {
			authorities.add(new Authority(parts.nextToken()));
		}
		
		UsernamePasswordAuthenticationToken userNamePasswordAuthToken =new  UsernamePasswordAuthenticationToken(
			userId,
			null,
			authorities
		);
		
		SecurityContextHolder.getContext().setAuthentication(userNamePasswordAuthToken);		
	}

	private void sendUnauthorizedReply(HttpServletResponse response) throws IOException {
		String UNAUTHORIZED = "Not autorized";
		ObjectMapper objectMapper = new ObjectMapper();
		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, UNAUTHORIZED);           
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

		try {
			PrintWriter out = response.getWriter();
			out.write(objectMapper.writeValueAsString(new Response(Map.ofEntries(Map.entry("Authorization", ErrorEnum.UNAUTHORIZED)), new ServletException(UNAUTHORIZED)))); // Empty
			out.flush();
		} catch (IOException e) {
			logger.error("Can't send UNAUTHORIZED response", e);
		}		
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		String authorization = request.getHeader("Authorization");
		String token;
		
		if(
			authorization != null &&		
			BEARER_AUTHORIZATION_PATTERN.matcher(authorization).matches()		
		) {
			token = readToken(authorization.substring(authorization.indexOf(' ')).trim());
			if(token != null) {
				setAuthentication(token);
			} else {
				sendUnauthorizedReply(response);			
				return;
			}

		}

		filterChain.doFilter(request, response);
	}

}
