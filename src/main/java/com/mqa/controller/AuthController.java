package com.mqa.controller;

import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import com.mqa.model.User;
import com.mqa.service.UserManagerService;
import com.mqa.service.dto.PasswordSubmissionDTO;
import com.mqa.util.ErrorEnum;
import com.mqa.util.Formatters;
import com.mqa.util.Validators;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin(origins = "*", maxAge = 3600)
@Controller
@RequestMapping("service/auth")
public class AuthController extends AbstractController  {
    private static final Logger logger = Logger.getLogger(AuthController.class);
    public static final String SIGNIN_URI = "/login";
    public static final String AUTH_CONTROLLER = "auth";
    private static final Pattern BASIC_AUTHORIZATION_PATTERN = Pattern.compile("^(B|b)asic (\\w|=)+"); 
    private static final String CHANGE_PASSWORD_URI = "/change-password";
    public static final String INVALID_NEW_PASSWORD = "Cannot change password because new password is null/empty or not strong enough.";

	@Autowired
	UserManagerService userManagementService;
    
	@Autowired
	AuthControllerConfig authConfig;


	private String createToken(String authorization) throws Exception  {
		String clearText = new String(Base64.getDecoder().decode(authorization.substring(authorization.indexOf(' ')+1).getBytes()));
		int pos = clearText.indexOf(':');
		
		if(!(pos > 0 && pos < clearText.length())) {
			return null;
		}

		String username = clearText.substring(0, pos);
		String password = clearText.substring(pos+1, clearText.length());
        User user = userManagementService.findById(username);

        if(user == null)
            return null;

        byte[] userPasswordHMAC = user.getPassword();
        byte[] passwordHMAC = Formatters.encodePassword(
            password.getBytes(userManagementService.getPasswordCharset()), 
            user.getSalt()
        );

        if(!Arrays.equals(passwordHMAC, userPasswordHMAC)) {
            return null;
        }

        String clearToken = String.format("%s|%s|%s", user.getName(), Calendar.getInstance().getTime().toString(), user.getRole());
        return clearToken + '|' + authConfig.tokenFactory().create(clearToken);
	}


	@GetMapping(SIGNIN_URI)
	public ResponseEntity<String> login(@RequestHeader("Authorization") String authorization
	) {
		String token;

		try {
            if(
                authorization == null ||				
                (authorization = authorization.trim()).length() == 0 ||
                !(
                        BASIC_AUTHORIZATION_PATTERN.matcher(authorization).matches() &&
                        (token = createToken(authorization)) != null
                )				
            )
                return ResponseEntity
                        .status(HttpStatus.UNAUTHORIZED)
                        .body("Can't validate credentials " + authorization);			
                
            return ResponseEntity.ok(token);
        }catch(Exception e) {
			logger.error("Can't authenticate user", e);
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(e.getMessage());

        }
		
	}

//    public static Map<String, ErrorEnum> validatePassword(String name, byte[] value, Map<String,ErrorEnum> validationMessages)

	@PostMapping(CHANGE_PASSWORD_URI)
	public ResponseEntity<Response> changePassword(@RequestBody PasswordSubmissionDTO body) {
        byte[] password1 = Formatters.passwordBytes(body.getPassword1());
        byte[] password2 = Formatters.passwordBytes(body.getPassword2());

        Map<String, ErrorEnum> validationMessages = Validators.validatePasswords(PASSWORD1_KEYWORD, PASSWORD2_KEYWORD, password1, password2, new HashMap<String,ErrorEnum>());
        
        try {
            User current = userManagementService.findById(body.getName());
            String token  = body.getToken();

            if (validationMessages.size() == 0) {
                if (current == null || !Validators.provided(token)) {
                    return ResponseEntity
                        .status(HttpStatus.NOT_FOUND)
                        .body(new Response(Map.of(NAME_KEYWORD, NOTEXISTING_ENTITY_MSG)));   
                }

                String decodedToken = authConfig.tokenFactory().decode(token);                
                String name = (new StringTokenizer(decodedToken, "|")).nextToken();

                if(!name.equals(body.getName())) {
                    return ResponseEntity
                        .status(HttpStatus.CONFLICT)
                        .body(new Response(Map.of(NAME_KEYWORD, NOTEXISTING_ENTITY_MSG)));                       
                }

                userManagementService.changePassword(current.getName(), password1);

                return ResponseEntity.status(HttpStatus.OK).body(new Response(body));
            }

            return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(
                    new Response(validationMessages, INVALID_NEW_PASSWORD)
                );

        }catch(Exception e) {
            return reportException(e, null, PERSISTENCE_EXCPT_MESG);   
        }
    }
}
