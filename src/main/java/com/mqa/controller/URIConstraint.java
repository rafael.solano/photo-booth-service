package com.mqa.controller;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import org.springframework.http.HttpMethod;

public class URIConstraint implements Serializable, Comparable<URIConstraint>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2843719447031339934L;
	private static final String PATH_PATTERN_EXPR = "^.+;(\\s*(POST|GET|PUT|DELETE)(\\s*,\\s*(POST|GET|PUT|DELETE){1,3}){0,3}|\\*)\\s*$";
	private static final Pattern PATH_PATTERN = Pattern.compile(PATH_PATTERN_EXPR);
	private static final Set<HttpMethod> ALL_ACTIONS;
	
	String path;
	Set<String> roles;
	Set<HttpMethod> actions;
	 
	static {
		ALL_ACTIONS = Collections.unmodifiableSet(
			new HashSet<HttpMethod>() {{
				add(HttpMethod.POST);
				add(HttpMethod.GET);
				add(HttpMethod.PUT);
				add(HttpMethod.DELETE);
			}}
		);
	}
	
	public URIConstraint(String key, String roles) throws ParseException {
		Set<String> roleSet = new HashSet<String>();
		Set<HttpMethod> actionSet = new HashSet<HttpMethod>();
		
		if(!PATH_PATTERN.matcher(key).matches())
			throw new ParseException(String.format("%s is not a valid key", key), 0);
		
		String path = key.substring(0, key.indexOf(';'));
		for(String action : key.substring(key.indexOf(';')+1).split("\\,")) {
			
			if(action.charAt(0) == '*') {
				actionSet.clear();
				actionSet.addAll(ALL_ACTIONS);				
				break;
				
			} else {
				actionSet.add(HttpMethod.valueOf(action.trim()));
			}
		}
		
		for(String role: roles.split(",")) {
			roleSet.add(role);
		}
		
		this.actions = Collections.unmodifiableSet(actionSet);
		this.roles = Collections.unmodifiableSet(roleSet);
		this.path = path;
		
	}

	public String getPath() {
		return path;
	}

	public Set<String> getRoles() {
		return roles;
	}

	public Set<HttpMethod> getActions() {
		return actions;
	}

	@Override
	public String toString() {
		return "URIConstraint [path=" + getPath()+ ", roles=" + getRoles() + ", actions=" + getActions() + "]";
	}


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        URIConstraint other = (URIConstraint) obj;
        return this.toString().equals(other.toString());
    }

    @Override
    public int compareTo(URIConstraint o) {
        return toString().compareTo(o.toString());
    }
	
    
}
