package com.mqa.controller;

import java.util.List;
import java.util.Map;

import com.mqa.model.Photo;
import com.mqa.service.PhotoService;
import com.mqa.service.UserManagerService;
import com.mqa.service.dto.PhotoSubmissionDTO;
import com.mqa.util.ErrorEnum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@CrossOrigin(origins = "*", maxAge = 3600,  methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.PUT, RequestMethod.DELETE, RequestMethod.OPTIONS, RequestMethod.HEAD})
@Controller
@RequestMapping("service/album")  
public class PhotoController extends AbstractController  {
    public static final int DEFAULT_PAGE_SIZE  = 10;

    public static final String PHOTO_URI="/photo";
    public static final String PHOTOS_URI="/photos";
    public static final String PHOTO_BY_ID_URI= PHOTO_URI + "/{id}";

    @Autowired
    UserManagerService userManagementService;
    
    @Autowired
    PhotoService photoService;
 
    @GetMapping(PHOTO_BY_ID_URI)
    public ResponseEntity<Response> get(@PathVariable("id") long id) {

        try {
            
            Photo photo = photoService.find(id);
            if (photo == null) {
                return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new Response(Map.of(ID_KEYWORD, NOTEXISTING_ENTITY_MSG)));    
            }


            return ResponseEntity.ok().body(new Response(photo));
        }catch(Exception e) {
            return reportException(e, null, PERSISTENCE_EXCPT_MESG);       
        }
    }     

    @PostMapping(PHOTO_URI)
    public ResponseEntity<Response> post(@RequestBody PhotoSubmissionDTO submittedPhoto) {        

        try {            
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String userName = authentication.getName();
            PhotoSubmissionDTO body = new PhotoSubmissionDTO(userName, submittedPhoto.getRawBytes(), submittedPhoto.getName());
            Photo photo = PhotoSubmissionDTO.toPhoto(userManagementService, body);
            Map<String, ErrorEnum> validationMessages = photoService.validate(photo);

            if(validationMessages.size() == 0) {
                photo.setId(photoService.insertPhoto(photo));
                return ResponseEntity.status(HttpStatus.CREATED).body(new Response(photo)); 
            }

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new Response(validationMessages, body));

        }catch(Exception e) {
            return reportException(e, submittedPhoto, PERSISTENCE_EXCPT_MESG);
        }
    }    

    @DeleteMapping(PHOTO_BY_ID_URI)
    public ResponseEntity<Response> delete(@PathVariable("id") long id) {

        try {
            Photo photo = photoService.deletePhoto(id);
            if(photo != null) {
                return ResponseEntity.status(HttpStatus.OK).body(new Response(id));
            }

            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(new Response(id));
        }catch(Exception e) {
            return reportException(e, null, PERSISTENCE_EXCPT_MESG);       
        }
    }     

    @GetMapping(PHOTOS_URI)
    public ResponseEntity<Response> all(Authentication authentication) {

        try {
            String userName = authentication.getName();
            List<Photo> photos = photoService.findPhotosByUserName(userName);
            
            return ResponseEntity.ok().body(new Response(photos));
        }catch(Exception e) {
            return reportException(e, null, PERSISTENCE_EXCPT_MESG);       
        }
    }  
}
