package com.mqa.controller;

import java.io.IOException;

import com.mqa.util.Properties;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin(origins = "*", maxAge = 3600)
@Controller
@RequestMapping("service/build-info")
public class BuildInfoController {
    private static final String BUILD_STAMP = "/build-stamp";
    Properties properties;
    String buildStamp;

    public BuildInfoController() throws IOException {
        properties = Properties.readResource(this.getClass());
        buildStamp = properties.getProperty("version") + '.' +
            properties.getProperty("revision") + '.' +
            properties.getProperty("fix") + '.' +
            properties.getProperty("build");
    }

    @GetMapping(BUILD_STAMP)
    public ResponseEntity<String> getBuildStamp() {
        return ResponseEntity.status(HttpStatus.OK).body(buildStamp); 
       
    } 
}
