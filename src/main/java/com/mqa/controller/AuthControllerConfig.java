package com.mqa.controller;


import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import com.mqa.security.AESClient;
import com.mqa.security.RSAClient;
import com.mqa.security.TokenFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AuthControllerConfig {
	RSAClient rsa;
	AESClient aes;
	TokenFactory tokenFactory;

	private static RSAClient staticRSA;
	private static AESClient staticAES;
	static {
		try {
			staticRSA = new RSAClient();
			staticAES = new AESClient();
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
	}

	public AuthControllerConfig() throws NoSuchAlgorithmException, UnsupportedEncodingException {
		// rsa = new RSAClient();
		// aes = new AESClient();
		rsa = staticRSA;
		aes = staticAES;		
		tokenFactory = new TokenFactory(rsa, aes);
	}
	
	@Bean
	public RSAClient rsa() {
		return rsa;
	}
	
	@Bean
	public AESClient aes() {
		return aes;
	}	
	
	@Bean
	public TokenFactory tokenFactory() {
		return tokenFactory;
	}
}
