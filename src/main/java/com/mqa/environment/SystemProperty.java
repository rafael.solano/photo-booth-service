package com.mqa.environment;

import org.apache.log4j.Logger;

public interface SystemProperty {
	public static final Logger logger = Logger.getLogger(SystemProperty.class);
	public static String NOT_PROVIDED_MESSAGE = "Property %s was not provided. Using default value=%s";
	public static String PROVIDED_MESSAGE = "%s=%s";
	
	public static int get(String key, int default_ ) {
		String value = System.getProperty(key);
		int result;
		
		if(value == null) {
			logger.warn(String.format(NOT_PROVIDED_MESSAGE, key, default_));
			return default_;
		}
		
		try {
			result = Integer.valueOf(value);
			logger.info(String.format(String.format(PROVIDED_MESSAGE, key, result)));
			return result;
			
		}catch(NumberFormatException e) {
			logger.warn(String.format(NOT_PROVIDED_MESSAGE, key, default_), e);
			return default_;
		}
	}
	
	public static String get(String key, String default_ ) {
		String value = System.getProperty(key);
		
		if(value == null) {
			logger.warn(String.format(NOT_PROVIDED_MESSAGE, key, default_));
			return default_;
		}
		
		logger.info(String.format(String.format(PROVIDED_MESSAGE, key, value)));
		return value;
	}

	public static int getEnv(String key, int default_ ) {
		String value = System.getenv(key);
		int result;
		
		if(value == null) {
			logger.warn(String.format(NOT_PROVIDED_MESSAGE, key, default_));
			return default_;
		}
		
		try {
			result = Integer.valueOf(value);
			logger.info(String.format(String.format(PROVIDED_MESSAGE, key, result)));
			return result;
			
		}catch(NumberFormatException e) {
			logger.warn(String.format(NOT_PROVIDED_MESSAGE, key, default_), e);
			return default_;
		}
	}
	
	public static String getEnv(String key, String default_ ) {
		String value = System.getenv(key);
		
		if(value == null) {
			logger.warn(String.format(NOT_PROVIDED_MESSAGE, key, default_));
			return default_;
		}
		
		logger.info(String.format(String.format(PROVIDED_MESSAGE, key, value)));
		return value;
	}	
}
