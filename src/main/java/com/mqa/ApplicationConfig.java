package com.mqa;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import com.mqa.controller.AuthController;
import com.mqa.controller.AuthenticationEntryPoint;
import com.mqa.controller.AuthenticationRequestFilter;
import com.mqa.controller.URIConstraint;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
public class ApplicationConfig extends WebSecurityConfigurerAdapter {
	private static Logger logger =Logger.getLogger(ApplicationConfig.class);
	@Autowired
	private AuthenticationEntryPoint authenticationEntryPoint;
	
	@Autowired
	private AuthenticationRequestFilter requestFilter;

	public List<URIConstraint> uriConstraints() {
		Properties properties = new Properties();
		InputStream input = this.getClass().getResourceAsStream("/constraints.properties");
		
		try {
			properties.load(input);			
		} catch (IOException e) {
			logger.warn("Can't read security constraint from /com/sqlrest/security/constraints.properties resource file", e);
		}
		
		List<URIConstraint> list =  properties.keySet()
				.stream()
				.map(key -> {
					try {
						return new URIConstraint(key.toString(), properties.getProperty(key.toString()));
					} catch (ParseException e) {
						logger.warn("Can't parse " + key + " url", e);;
						return null;
					}			
				})
				.filter(filter -> filter != null)
				.collect(Collectors.toList());
		return list;
	}
	

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception
    {
		List<URIConstraint> uriConstraints = this.uriConstraints();
		httpSecurity
			.csrf()
			.disable()
			.authorizeRequests()
			.antMatchers('/' + AuthController.AUTH_CONTROLLER + AuthController.SIGNIN_URI)
			.permitAll();
			
		uriConstraints.forEach(constraint -> {
			String[] roles =  constraint.getRoles().toArray(new String[constraint.getRoles().size()]);
			
			constraint.getActions().forEach( method -> {
				try {
					httpSecurity.authorizeRequests().antMatchers(method, constraint.getPath()).hasAnyAuthority(roles);
					logger.debug(String.format("Authorized %s request for %s to %s", method, constraint.getPath(), constraint.getRoles()));
				} catch (Exception e) {
					logger.warn(String.format("Can't authorize %s requests for %s path", constraint.getActions(), constraint.getPath()), e);
				}
			});
		});
		
		httpSecurity
			.exceptionHandling()
			.authenticationEntryPoint(authenticationEntryPoint)
			.and()
			.sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		httpSecurity.addFilterBefore(requestFilter, UsernamePasswordAuthenticationFilter.class);

    }    

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return source;
    }    
}
