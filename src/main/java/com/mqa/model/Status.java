package com.mqa.model;

public class Status {
    String id;
    String name;

    public Status() {
        
    }
    public Status(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;        
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
        
}
