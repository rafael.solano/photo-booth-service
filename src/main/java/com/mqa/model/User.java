package com.mqa.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Lob;
import org.hibernate.annotations.Nationalized;

@Entity(name="User")
@Table(name = "APP_USER")
public class User  {
    
    @Id
    @Column(name = "USER_NAME", nullable = false)   
    @Nationalized 
    String name; // 

    @Column(name = "USER_PASSWORD", nullable = false)  
    @Lob
    byte[] password; // 

    @Column(name = "SALT", nullable = false)    
    @Lob
    byte[] salt; // 

    @Column(name = "LAST_NAME", nullable = false)        
    @Nationalized 
    String lastName; // 

    @Column(name = "USER_STATUS", nullable = false)        
    @Nationalized 
    String status; // 

    @Column(name = "FIRST_NAME", nullable = false)        
    @Nationalized 
    String firstName; // 

    @Column(name = "USER_ROLE", nullable = false)        
    @Nationalized 
    String role; // 

    @Column(name = "CREATION_DATE", nullable = false)        
    Timestamp creationDate; //     
    
    public User() {
    }

       public User(String name, byte[] password, byte[] salt, String lastName, String firstName, String status, String role, Timestamp creationDate) {
        this.name = name;
        this.password = password;
        this.salt = salt;
        this.lastName = lastName;
        this.status = status;
        this.firstName = firstName;
        this.role = role;
        this.creationDate = creationDate;
    }    

    public String getName() {
        return name;
    }

    public void setName(String name)  {
        this.name = name;
    }

    public byte[] getPassword() {
        return password;
    }

    public void setPassword(byte[] password) {
        this.password = password;
    }

    public byte[] getSalt() {
        return salt;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName)  {
        this.lastName = lastName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status)  {
        this.status = status;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role)  {
        this.role = role;
    }

    public Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;

        return this.toString().equals(other.toString());
    }

    @Override
    public String toString() {
        return "User [firstName=" + getFirstName() + ", lastName="+ getLastName() + ", name=" + getName() + ", role=" + getRole() +  ", status=" + getStatus() + "]";
    }
}
