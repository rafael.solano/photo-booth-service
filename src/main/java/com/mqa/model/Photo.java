package com.mqa.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Nationalized;

@Entity(name="Photo")
@Table(name = "PHOTO")
@SqlResultSetMappings ({
    @SqlResultSetMapping(name="PhotoIdDTOMapping", 
        classes={ 
            @ConstructorResult( //long id, Timestamp creationDate, String name
                columns={
                    @ColumnResult(name="id", type = long.class),                    
                    @ColumnResult(name="name", type = String.class),
                },
                targetClass = Photo.class
            )
        }
    )
})
@NamedNativeQueries({
    @NamedNativeQuery(
        name = "findPhotosByUserName",
        query = "SELECT PHOTO_NAME name, PHOTO_ID id FROM PHOTO WHERE USER_NAME=?1",
        resultClass = Photo.class,
        resultSetMapping = "PhotoIdDTOMapping"
    )
})
public class Photo {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column(name = "PHOTO_ID")
    private Long id;

    @Column(name = "CREATION_DATE", nullable = false)
    Timestamp creationDate;
    
    @Column(name = "RAW_BYTES", nullable = false)
    @Lob
    byte[] rawBytes;

    @Column(name = "PHOTO_NAME", nullable = false)
    @Nationalized
    String name;

    @ManyToOne(optional = false)   
    @Nationalized
    @JoinColumn(name="USER_NAME", nullable=false) 
    User user;

    @Version
    @Column(name = "VERSION", nullable = false)
    private Long version;
        
    public Photo() {        
    }

    
    
    public Photo(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Photo(Timestamp creationDate, byte[] rawBytes, String name, User user) {
        this.creationDate = creationDate;
        this.rawBytes = rawBytes;
        this.name = name;
        this.user = user;
    }



    public Long getId() {
        return id;
    }



    public void setId(Long id) {
        this.id = id;
    }



    public Timestamp getCreationDate() {
        return creationDate;
    }



    public void setCreationDate(Timestamp creationDate) {
        this.creationDate = creationDate;
    }



    public byte[] getRawBytes() {
        return rawBytes;
    }



    public void setRawBytes(byte[] rawBytes) {
        this.rawBytes = rawBytes;
    }



    public String getName() {
        return name;
    }



    public void setName(String name) {
        this.name = name;
    }



    public User getUser() {
        return user;
    }



    public void setUser(User user) {
        this.user = user;
    }



    public Long getVersion() {
        return version;
    }



    public void setVersion(Long version) {
        this.version = version;
    }

    
}
