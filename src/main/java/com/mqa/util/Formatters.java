package com.mqa.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.Logger;

public class Formatters {
    private static Logger logger = Logger.getLogger(Formatters.class);
    public static final SimpleDateFormat timestampFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");    
    public static final String HMAC_SHA512 = "HmacSHA512";
    public static final Charset passwordCharset =  Charset.forName("UTF-8");;


    public static final String urlEncode(String text) {
        try {
            return URLEncoder.encode(text,  StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            logger.warn("Can't encode string: " + text, e);
            return null;
        }
    }

    public static final String urlEncode(Timestamp timestamp) {
        return urlEncode(timestampFormatter.format(timestamp));
    }

    public static final Timestamp parseTimestamp(String timestamp) {
        try {
            return new Timestamp(timestampFormatter.parse(timestamp).getTime());
        } catch (ParseException e) {
            logger.warn("Can't encode timestamp string: " + timestamp, e);
            return null;
        }
    }

    
    public static byte[] encodePassword(byte[] password, byte[] key) throws SignatureException, NoSuchAlgorithmException, InvalidKeyException
    {
        if(password == null)
            return null;
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, HMAC_SHA512);
        Mac mac = Mac.getInstance(HMAC_SHA512);        
        mac.init(secretKeySpec);
        return mac.doFinal(password);
    }


    public static byte[] encodePassword(String password, byte[] key) throws SignatureException, NoSuchAlgorithmException, InvalidKeyException
    {
        if(password == null)
            return null;
        return encodePassword(password.getBytes(passwordCharset), key);
    }

    public static byte[] passwordBytes(String password)
    {
        if(password == null)
            return null;
        return password.getBytes(passwordCharset);
    }    

    public static String passwordString(byte[] buffer) {
        if(buffer == null)
            return null;
        return new String(buffer, passwordCharset);
    }

}
