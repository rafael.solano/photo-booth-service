package com.mqa.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class Validators {
    public static final Pattern UNSIGNED_INT_PATTERN = Pattern.compile("\\d+");
    public static final Pattern STRONG_PASSWORD_PATTERN = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*])(?=.{8,})");
    public static final String EMPTY_STRING  = "";
    private static final byte[] EMPTY_BYTE_ARRAY = {};

    public static String trim(String string) {
        if(string == null || string.isEmpty() || string.isBlank()) {
            return EMPTY_STRING;
        }

        return string.trim();
    }
    
    private static int passwordStrength(String password){
        
        //total score of password
        int iPasswordScore = 0;
        
        if( password.length() < 8 )
            return 0;
        else if( password.length() >= 10 )
            iPasswordScore += 2;
        else 
            iPasswordScore += 1;
        
        //if it contains one digit, add 2 to total score
        if( password.matches("(?=.*[0-9]).*") )
            iPasswordScore += 2;
        
        //if it contains one lower case letter, add 2 to total score
        if( password.matches("(?=.*[a-z]).*") )
            iPasswordScore += 2;
        
        //if it contains one upper case letter, add 2 to total score
        if( password.matches("(?=.*[A-Z]).*") )
            iPasswordScore += 2;    
        
        //if it contains one special character, add 2 to total score
        if( password.matches("(?=.*[~!@#$%^&*()_-]).*") )
            iPasswordScore += 2;
        
        return iPasswordScore;
        
    }

    public static boolean provided(String value) {
        return !(value == null || value.isEmpty() || value.isBlank());
    }
    public static Map<String, ErrorEnum> validateString(String name, String value, Map<String,ErrorEnum> validationMessages) {
        if(!provided(value)) {
            validationMessages = new HashMap<String, ErrorEnum>(validationMessages);
            validationMessages.put(name, ErrorEnum.EMPTY_STRING);
        }
        return validationMessages;
    }

    public static Map<String, ErrorEnum> validateBytes(String name, byte[] value, Map<String,ErrorEnum> validationMessages) {
        if(value == null) {
            validationMessages = new HashMap<String, ErrorEnum>(validationMessages);
            validationMessages.put(name, ErrorEnum.EMPTY_STRING);
        }
        return validationMessages;
    }    

    public static Map<String, ErrorEnum> validatePassword(String name, byte[] value, Map<String,ErrorEnum> validationMessages) {
        if(value == null || value.length == 0) {
            validationMessages = new HashMap<String, ErrorEnum>(validationMessages);
            validationMessages.put(name, ErrorEnum.UNSAFE_PASSWORD);
        } else {
            String password = Formatters.passwordString(value);

            if(passwordStrength(password) < 5) {
                validationMessages = new HashMap<String, ErrorEnum>(validationMessages);
                validationMessages.put(name, ErrorEnum.UNSAFE_PASSWORD);                
            }
        }
        
        return validationMessages;
    } 

    public static Map<String, ErrorEnum> validatePasswords(
        String name1, 
        String name2, 
        byte[] password1, 
        byte[] password2, 
        Map<String,ErrorEnum> validationMessages
    ) {        
        int size = validationMessages.size();
        
        validationMessages = validatePassword(name1, password1, validationMessages);
        password2 = password2 == null ? EMPTY_BYTE_ARRAY : password2;

        if(validationMessages.size() > size) {
            return validationMessages;
        }

        if(!Arrays.equals(password1, password2)) {
            validationMessages = new HashMap<String, ErrorEnum>(validationMessages);
            validationMessages.put(name1, ErrorEnum.UNSAFE_PASSWORD);   
            validationMessages.put(name2, ErrorEnum.UNSAFE_PASSWORD);   
        }

        return validationMessages;
    }

    public static class ChoiceValidator<T> {
        public static <T> Map<String,ErrorEnum> validateChoice(String name, T value, List<T> choices, Map<String,ErrorEnum> validationMessages) {
            if(value == null || choices.contains(value) == false) {
                validationMessages = new HashMap<String, ErrorEnum>(validationMessages);
                validationMessages.put(name, ErrorEnum.MISSING_CHOICE);
            }

            return validationMessages;
        }
    }

    public static boolean equals(String a, String b) {
        if(a == b)
            return true;

        if(!(a == null || b == null))
            return a.equals(b);

        return false;
    }
}
