package com.mqa.util;

public class Triplet<A,B,C> {
    public final A a;
    public final B b;
    public final C c;

    public Triplet(A a, B b, C c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public static <A, B, C>  Triplet<A, B, C> of(A a, B b, C c) {
        return new Triplet<A, B, C> (a, b, c);
    }
}
