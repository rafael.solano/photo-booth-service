package com.mqa.util;

import java.io.IOException;
import java.io.InputStream;

public class Properties extends java.util.Properties{
    public Properties() {
        super();        
    }

    public Properties(InputStream input) throws IOException {
        this.load(input);
    }

    public static Properties readResource(String resource) throws IOException {
        return new Properties(Properties.class.getResourceAsStream(resource));
    }

    public static Properties readResource(Class<?> clazz) throws IOException {
        return readResource('/'+clazz.getName().replace('.', '/')+".properties");
    }  
    

}
