package com.mqa.util;

import java.util.Collection;
import java.util.function.BiFunction;


public class Aggregator<A,T> {
    public static <A,T> A sequential(Collection<T> collection, BiFunction<A,T,A> reducer, A aggregator) {

        for(T element : collection) {
            aggregator = reducer.apply(aggregator, element);
        }

        return aggregator;
    }
}
