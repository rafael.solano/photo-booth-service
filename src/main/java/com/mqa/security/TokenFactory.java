package com.mqa.security;

import java.security.SecureRandom;

import org.apache.commons.lang3.RandomStringUtils;

public class TokenFactory {
	RSAClient rsa;
	AESClient aes;	

	public static String randomString(int length) {
		length = length < 16 ? 16 : length;
	    boolean useLetters = true;
	    boolean useNumbers = true;
	    String string = RandomStringUtils.random(length, useLetters, useNumbers);	    
	    return string;
	}
	
	public static byte[] randomBytes(int length) {
		length = length < 16 ? 16 : length;
		SecureRandom random = new SecureRandom();
	    byte[] bytes = new byte[length];
		random.nextBytes(bytes);

		return bytes;
	}

	
	public TokenFactory(RSAClient rsa, AESClient aes) {	
		this.rsa = rsa;
		this.aes = aes;
	}
	
	public String create(String content) throws Exception {
		String password = aes.randomPassword();
		String encoded = aes.encrypt(content, password);
		String encodedPassword = rsa.encrypt(password);
		return (encoded + ':' + encodedPassword).replaceAll("\\s+", "");
	}
	
	public String decode(String content) throws Exception {
		int p = content.indexOf(':');
		String encoded = content.substring(0, p);
		String password = rsa.decrypt(content.substring(p+1, content.length()));
		return aes.decrypt(encoded, password);
	}	
}
