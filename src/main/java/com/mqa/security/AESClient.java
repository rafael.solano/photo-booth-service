package com.mqa.security;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.mqa.environment.SystemProperty;

import org.apache.commons.pool2.impl.GenericObjectPool;

public class AESClient {
	GenericObjectPool<Cipher> cipherPool;
	IvParameterSpec iv;
	  
	public AESClient() throws UnsupportedEncodingException {
		cipherPool = new GenericObjectPool<Cipher>(new CipherFactory(CipherType.AES));
		cipherPool.setMaxIdle(SystemProperty.get("pool.cipher.aes.idle", 10));
		cipherPool.setMaxTotal(SystemProperty.get("pool.cipher.aes.total", 100));
		cipherPool.setMaxWaitMillis(SystemProperty.get("pool.cipher.aes.wait", 60000));
		iv = new IvParameterSpec(randomString().getBytes("UTF-8"));
		
	}
	
	
	public String randomString() {
		return TokenFactory.randomString(16);
	}
	
	public String randomPassword() {
		return randomString();
	}
	
	public String encrypt(String plainText, String password) throws Exception {
		String key = password;
		String value = plainText;
		SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
		Cipher cipher = cipherPool.borrowObject();
		
		try {			
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);		 
			byte[] encrypted = cipher.doFinal(value.getBytes());
			return  Base64.getMimeEncoder().encodeToString(encrypted);
		} finally {
			cipherPool.returnObject(cipher);
		}
	    
	}
	
	public String decrypt(String encoded, String password) throws Exception {
		String key = password;
        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES"); 
        Cipher cipher = cipherPool.borrowObject();
        
        try {
        	cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
        	byte[] original = cipher.doFinal(Base64.getMimeDecoder().decode(encoded));
        	return new String(original);
        } finally {
        	cipherPool.returnObject(cipher);
        }
	}
}
