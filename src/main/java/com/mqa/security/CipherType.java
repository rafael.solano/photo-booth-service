package com.mqa.security;

public enum CipherType {
	RSA("RSA"),
	AES("AES/CBC/PKCS5PADDING");
	
	private String name;
	
	private CipherType(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
		
}

