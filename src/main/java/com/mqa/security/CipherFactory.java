package com.mqa.security;

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

import javax.crypto.Cipher;
public class CipherFactory extends BasePooledObjectFactory<Cipher> {
	
	CipherType type;
	
	public CipherFactory(CipherType type) {
		this.type = type;
	}
	
	@Override
	public Cipher create() throws Exception {
		return Cipher.getInstance(type.toString());
	}

	@Override
	public PooledObject<Cipher> wrap(Cipher cipher) {
		return new DefaultPooledObject<Cipher>(cipher);
	}

    @Override
    public void passivateObject(PooledObject<Cipher> pooledObject) {
        
    }	
}
