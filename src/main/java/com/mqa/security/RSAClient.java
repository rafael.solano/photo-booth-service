package com.mqa.security;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;
import javax.crypto.Cipher;

import com.mqa.environment.SystemProperty;

import org.apache.commons.pool2.impl.GenericObjectPool;

public class RSAClient {
	KeyPair keyPair;
	PublicKey publicKey;
	PrivateKey privateKey;
	GenericObjectPool<Cipher> cipherPool;
	
	public RSAClient() throws NoSuchAlgorithmException {		
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");		
		keyPairGenerator.initialize(1024);
		keyPair = keyPairGenerator.genKeyPair();		
		publicKey = keyPair.getPublic();
		privateKey = keyPair.getPrivate();
		cipherPool = new GenericObjectPool<Cipher>(new CipherFactory(CipherType.RSA));
		cipherPool.setMaxIdle(SystemProperty.get("pool.cipher.rsa.idle", 10));
		cipherPool.setMaxTotal(SystemProperty.get("pool.cipher.rsa.total", 100));
		cipherPool.setMaxWaitMillis(SystemProperty.get("pool.cipher.rsa.wait", 60000));
	}
	
	
	public String publicKey() {
		return Base64.getMimeEncoder().encodeToString(publicKey.getEncoded());
	}
	
	public String privateKey() {
		return Base64.getMimeEncoder().encodeToString(privateKey.getEncoded());
	}	
	
	public String encrypt(String plainText) throws Exception {      
        Cipher cipher = cipherPool.borrowObject();
        
        try {
	        cipher.init(1, publicKey);
	        byte[] enBytes = cipher.doFinal(plainText.getBytes("UTF-8"));
	        return Base64.getMimeEncoder().encodeToString(enBytes);
        }finally {
        	cipherPool.returnObject(cipher);
        }
	}
	
    public String decrypt(String encodedText) throws Exception  {
        Cipher cipher = cipherPool.borrowObject();
        
        try {
	        cipher.init(2, privateKey);
	        byte[] deBytes = cipher.doFinal(Base64.getMimeDecoder().decode(encodedText));
	        return new String(deBytes);
        }finally {
        	cipherPool.returnObject(cipher);
        }
    }	
}
