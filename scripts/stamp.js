const path = require('path');
const fs = require('fs');
const readline = require('readline');
const scriptDir=`${__dirname}/..`;
const buildInfoPath = path.normalize(`${scriptDir}/src/main/resources/com/mqa/controller/BuildInfoController.properties`);
const buildInfo = fs.readFileSync(buildInfoPath)
    .toString()
    .split(/\r?\n/)
    .filter(line => line.trim().length > 0 ? true: true)
    .reduce((object, line) => {
        const parts = line.split('=');
        return {...object, [parts[0]]: parts[1]}
    }, {});

const updatedBuildInfo = {...buildInfo, build: parseInt(buildInfo.build) + 1};
const updatedBuildInfoText = Object.keys(updatedBuildInfo)
    .map(key => `${key}=${updatedBuildInfo[key]}`)
    .join('\r\n');

fs.writeFileSync(buildInfoPath, updatedBuildInfoText);
console.log(`INFO: New build stamp is ${updatedBuildInfo.version}.${updatedBuildInfo.revision}.${updatedBuildInfo.fix}.${updatedBuildInfo.build}`)