CREATE TABLE APP_USER (
	USER_NAME NVARCHAR(64) NOT NULL,
	USER_PASSWORD BLOB NOT NULL,
	USER_ROLE NVARCHAR(64) NOT NULL,
	USER_STATUS NVARCHAR(64) NOT NULL,		
	SALT BLOB NOT NULL,
	LAST_NAME NVARCHAR(64) NOT NULL,
	FIRST_NAME NVARCHAR(64) NOT NULL,
	CREATION_DATE TIMESTAMP NOT NULL,	
	CONSTRAINT APP_USER_PK PRIMARY KEY (USER_NAME)
);

CREATE TABLE PHOTO (
	PHOTO_ID bigint NOT NULL GENERATED BY DEFAULT AS IDENTITY,
	PHOTO_NAME NVARCHAR(64) NOT NULL,
	USER_NAME VARCHAR(256) NOT NULL,
	CREATION_DATE TIMESTAMP NOT NULL,
	RAW_BYTES BLOB NOT NULL,
	PHOTO_VERSION BIGINT DEFAULT 0 NOT NULL,
	VERSION BIGINT NOT NULL,
	CONSTRAINT PHOTO_PK PRIMARY KEY (PHOTO_ID),
	CONSTRAINT PHOTO_FK1 FOREIGN KEY (USER_NAME) REFERENCES APP_USER(USER_NAME)
);

SELECT USER_NAME userName, PHOTO_ID id FROM PHOTO WHERE USER_NAME=?1
SELECT USER_NAME userName, PHOTO_ID id, CREATION_DATE creationDate FROM PHOTO