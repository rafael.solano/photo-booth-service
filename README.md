# Photo Booth #

Springboot application used for demo and onboarding purposes

## Useful commands ##

### Fetche Latest Image ###

```
docker pull nemesysolano/photo-booth:latest
```

### Create credentials ###

```
kubectl create secret docker-registry nemesysolano --docker-username=nemesysolano --docker-password=976bd7d8-ede5-42ce-afcc-0fc43493a000 --docker-email=nemesysolano@gmail.com --docker-server=docker.index.io
```

### Open SSH Port ###

```
kubectl port-forward svc/photo-booth-service 8080:8080  
```

### Create Deployment and Service ###

```
kubectl apply -f k8s.yaml 
```
